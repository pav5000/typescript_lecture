
// ###### 1. Введение ######

// Разработчик - Андерс Хейлсберг (https://ru.wikipedia.org/wiki/Хейлсберг,_Андерс), за плечами Turbo Pascal, Delphi и C#.
// Первая версия представлена в 2012г.
// Лицензия Apache, опенсурс https://github.com/Microsoft/TypeScript

// ###### 2. Синтаксис ######

// Является надмножеством JavaScript, любой верный код на JS верен и в TS.
// Строгая типизация является опциональной.

// ## 2.1 Типы

// # 2.1.1

var anyval; // переменная типа "any", равноценна обычной переменной js

var num1: number; // числовой тип
num1 = 1;

var num2: number = 2; // тип + инициализация

var num3 = 3; // выведение типов, num3 получает тип number

// # 2.1.2

function getLength(x: string[]):number {
    return( x.length );
}

function toLog(msg: string):void {
    console.log('message: ' + msg);
}

getLength( ['John', 'Dan', 'Aaron', 'Fritz'] ); // ok

getLength( [{name: 'Aaron'}, {name: 'Fritz'}] ); // ошибка компиляции

// 2.2 Классы

// 2.2.1 Конструктор

// TS
class Vertex {
    x:number;
    y:number;
    
    constructor(x: number, y:number) {
        this.x = x;
        this.y = y;
    }
}
var v = new Vertex(2, 3);

// JS
var Vertex = (function () {
    function Vertex(x, y) {
        this.x = x;
        this.y = y;
    }
    return Vertex;
})();
var v = new Vertex(2, 3);

// 2.2.2 Объявление полей в конструкторе

class Vertex {
    constructor(public x: number, public y:number) {}
}
var v = new Vertex(2, 3);

// 2.2.3 Значение по-умолчанию

// TS
class Vertex {
    constructor(public x: number = 0, public y:number = 0) {}
}
var v = new Vertex(2, 3);

// JS
var Vertex = (function () {
    function Vertex(x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        this.x = x;
        this.y = y;
    }
    return Vertex;
})();
var v = new Vertex(2, 3);
