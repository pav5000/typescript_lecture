
class ImgStat {

    click_counter:number = 0;

    constructor(img:HTMLImageElement) {
        img.addEventListener('click', ()=> {
            this.click_counter++;
        })
    }

}
