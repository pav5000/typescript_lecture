
class Point {
    x:number;
    y:number;
}

class Circle extends Point {
    r:number;
}

// ###############################################################

interface PointData {
    x:number;
    y:number;
}

interface CircleData extends PointData {
    r:number;
}
