
function zipNumbers(
    f:(a:number, b:number)=>number,
    list1:number[],
    list2:number[]
):number[] {

    return(
        list1.map( (elem, i) => f(elem, list2[i]) )
    );

}

[2, 3, 4].forEach( (elem) => console.log(elem) );
