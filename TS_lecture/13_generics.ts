
function make_pair<T>(a:T, b:T):T[] {
    return([a, b]);
}

make_pair<number>(2, 3);
make_pair<string>('str1', 'str2');

interface Person {
    name:string;
    surname:string;
}

make_pair<Person>(
    {name:"Иван", surname:"Смирнов"},
    {name:"Петр", surname:"Сидоров"}
);
