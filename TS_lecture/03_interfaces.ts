
interface Person {
    name:string;
    surname:string;
}

var p:Person = {name:"Алексей", surname:"Сидоров"};

printFullName(p);
printFullName({name:"Иван", surname:"Смирнов"});

function printFullName( obj:Person ) {
    console.log(obj.name + " " + obj.surname);
}
