
function add(a:number, b:number):number {
    return(a+b);
}

function zipNumbers(
    f:(a:number, b:number)=>number,
    list1:number[],
    list2:number[]
):number[] {

    return(
        list1.map(
            function(elem, i) {
                return( f(elem, list2[i]) );
            }
        )
    );

}

zipNumbers(add, [1, 2, 3], [2, 3, 4]);
