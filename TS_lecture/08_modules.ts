
module Tools {
    var someInternalVar = 5;
    export var somePublicVar = 8;

    function someInternalFunc() {
        console.log(someInternalVar);
    }
    export function somePublicFunc() {
        console.log(someInternalVar);
    }
}

Tools.somePublicFunc();
