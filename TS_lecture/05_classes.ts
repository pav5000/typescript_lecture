
class Vector {
    constructor(
        private x:number = 0,
        private y:number = 0
    ) {}

    length():number {
        return(Math.sqrt(
                this.x*this.x + this.y*this.y
        ));
    }

    private internalDebug() {
        console.log(this.x + " " + this.y);
    }
}
var v = new Vector(2, 3);
console.log( v.length() );
